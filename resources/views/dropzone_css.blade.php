<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

<link rel="stylesheet" href="{{ asset('vendor/lybuneiv/dropzone/css/dropzone.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/lybuneiv/dropzone/css/custom.css') }}">
