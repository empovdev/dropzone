# Readme

Staff module.

## Installation

You can install the package using Composer:

```bash
composer require lybuneiv/dropzone
```

Then add the service provider to config/app.php:

```bash
Lybuneiv\Dropzone\DropzoneServiceProvider::class
```

Publish the config file:

```bash
php artisan vendor:publish --provider="Lybuneiv\Dropzone\DropzoneServiceProvider" --tag="config"
```
Publish the public file:

```bash
php artisan vendor:publish --provider="Lybuneiv\Dropzone\DropzoneServiceProvider" --tag="public"
```

Publish the view file:

```bash
php artisan vendor:publish --provider="Lybuneiv\Dropzone\DropzoneServiceProvider" --tag="views"
```


Then symlink storage link:

```bash
php artisan storage:link
```

## Usage

Add dropzone style in header

```bash
{!! Dropzone::renderDropzoneCss() !!}
```

Add dropzone script

```bash
{!! Dropzone::renderDropzoneJs() !!}
```


Single image html

```bash
{!! Dropzone::renderSingle($url_image, 'input_name', 'class_name') !!}
```

Single image js

```bash
{!! Dropzone::renderSingleJs('class_name') !!}
```

Multiple image html

```bash
{!! Dropzone::renderMultiple($array_image, 'input_name', 'class_name') !!}
```

Multiple image js

```bash
{!! Dropzone::renderMultipleJs('class_name') !!}
```