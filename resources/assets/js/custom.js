var base_url = $('.base_url').val();

function addCaptionImage(obj = '', classname= 'hidden_caption1')
{
    var el = $(obj);
    var value = el.val();
    $('.'+classname).val(value);
}

function removeImageDropzone(obj) {
    var el = $(obj);
    var name = el.attr('data-name');
    var rootElement = $('.hidden_file[data-value="'+name+'"]');
    var _token = $('input[name="_token"]').val();
    var filename = rootElement.find('.hidden_value').val();
    var html = generateDefautlImage();
    el.closest('.dropzone_single').html(html);
    rootElement.remove();
    el.closest('.dz-preview').remove();

}

function triggerAddImageSingle(class_name = '')
{
    var el = $('.'+class_name);
    console.log(el);
    el.closest('.single_dropzone').trigger('click');
}

function triggerAddImageMultiple(class_name = '')
{
    var el = $('.'+class_name);
    console.log(el);
    el.closest('.dz-clickable').trigger('click');
}

$(function(){
    $('.multiple_dropzone > .not_clickable_dropzone').on('click', function(e){
        e.stopPropagation();
        e.preventDefault();
    });
});



function generateDefautlImage()
{
    var html = '';

    html += '<div class="dz-preview dz-processing dz-image-preview dz-success dz-complete exist_image">';
    html += '<div class="dz-image">';
    html += '<img data-dz-thumbnail="" src="' + base_url + '/vendor/lybuneiv/dropzone/img/default.jpg" style="width: 100%;">';
    html += '</div>';
    html += '<div class="dz-details">';
    html += '<div class="dz-filename"><span data-dz-name="">default.png</span></div>';
    html += '</span></div>';
    html += '<div class="dz-size" data-dz-size=""></div>';
    html += '</div>';
    html += '<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>';
    html += '<div class="dz-success-mark"><span></span></div>';
    html += '<div class="dz-error-mark"><span></span></div>';
    html += '<div class="dz-error-message"><span data-dz-errormessage=""></span></div>';
    html += '</div>';

    return html;
}

function generateOption(el = '', class_name = '')
{
    var text = '';   
    var dropzone_url = base_url + '/admin/image/upload-single/'+class_name 
    var option = {
        uploadMultiple: false,
        parallelUploads: 1,
        maxFiles: 1,
        addRemoveLinks: true,
        autoProcessQueue :true,
        dictDefaultMessage: text,
        acceptedFiles: 'image/png,.jpg,.jpeg',
        clickable: '.dev_'+class_name+' .single_dropzone',
        url: dropzone_url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: function() {
            var myDropzone = this;
            this.on("success", function(files, response) {
                el.closest('.mydropzone').find('.photo').val(response.name);
                el.find('.dz-size').addClass('display_none');
            });
            this.on("error", function(files, response) {
              console.log(response);
            });
            this.on("removedfile", function(file){
                var name = file.name;
                var rootElement = $('.hidden_file[data-value="'+name+'"]');
                var _token = $('input[name="_token"]').val();
                var filename = rootElement.find('.hidden_value').val();
                el.closest('.mydropzone').find('.photo').val('');
                el.find('.hidden_caption1').val('');
                el.find('.hidden_caption2').val('');
                var html = generateDefautlImage();
                if(this.files.length == 0)
                    el.html(html);
             });
            this.on('addedfile', function(file) {
                el.find('.exist_image').remove();
                if (this.files.length > 1) {
                    console.log('ss');
                  this.removeFile(this.files[0]);
                }
            });

            this.on("maxfilesexceeded", function(file){
                // if (this.files.length > 1) {
                //   this.removeFile(this.files[0]);
                // }
                alert("No more files please!");
            });
        }
    };

    return option;
}

function generateOptionMutiple(el = '', class_name = '')
{
    var text = 'Drop files here to upload';
    var count = $('.dev_'+class_name).find('.count_item').val();
    if(count > 0)
        text = '';

    var input_name = el.closest('.mydropzone').find('.input_name').val();
    var dropzone_url = base_url + '/admin/image/upload-multiple/'+class_name 
    var option = {
        uploadMultiple: true,
        parallelUploads: 1,
        maxFiles: 30,
        addRemoveLinks: true,
        autoProcessQueue :true,
        dictDefaultMessage: text,
        acceptedFiles: 'image/png,.jpg,.jpeg',
        // clickable: '.dev_'+class_name+' .multiple_dropzone > :not(.not_clickable_dropzone)',
        url: dropzone_url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: function() {
            var myDropzone = this;


            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function() {
              // Gets triggered when the form is actually being sent.
              // Hide the success button or the complete form.
            });
            this.on("successmultiple", function(files, response) {
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.

                var html = '';
                var caption = '';
                $.each( files, function( key, val ) {

                    var value = response.filename[key];
                    caption = '';
                    html += '<div class="hidden_file" data-value="'+value['fullname']+'">';
                    html += '<input type="hidden" value="" name="img_id[]">';
                    html += '<input type="hidden" value="'+value['name']+'" name="'+input_name+'[]" class="hidden_value">';
                    // html += '<input type="hidden" value="" name="img_caption1[]" class="hidden_multiple_caption1">';
                    // html += '<input type="hidden" value="" name="img_caption2[]" class="hidden_multiple_caption2">';
                    html += '</div>';
                    // caption += '<div class="input-field col s12">';
                    // caption += '<input data-class="hidden_multiple_caption1" type="text" value="" name="tmp_img_caption1[]" class="tmp_hidden_caption1" onblur="addCaptionImageMutiple(this)" data-name="'+value['fullname']+'">';
                    // caption += '<label>Caption 1</label>';
                    // caption += '</div>';
                    // caption += '<div class="input-field col s12">';
                    // caption += '<input data-class="hidden_multiple_caption2" type="text" value="" name="tmp_img_caption2[]" class="tmp_hidden_caption2" onblur="addCaptionImageMutiple(this)" data-name="'+value['fullname']+'">';
                    // caption += '<label>Caption 2</label>';
                    // caption += '</div>';
                    var rootImage = $(val.previewElement);
                    var radio = '<label><input class="not_clickable_dropzone" type="radio" value="'+value['name']+'" name="main" onChange="changeMain(this);"> Main</label>';
                    rootImage.find('.dz-details').find('.dz-filename').find('span').html(radio);
                    rootImage.append(html);
                });

                $('.append_file_name').append(html);
                $('.dz-size').addClass('display_none');
                $('.dz-message').remove();

            });
            this.on("errormultiple", function(files, response) {

              console.log(response);
            });
            this.on("removedfile", function(file){

                var name = file.name;

                var rootElement = $('.hidden_file[data-value="'+name+'"]');
                var _token = $('input[name="_token"]').val();
                var filename = rootElement.find('.hidden_value').val('');
                rootElement.remove();

             });

        }
    };

    return option;
}


