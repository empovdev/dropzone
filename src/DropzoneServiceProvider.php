<?php

namespace Lybuneiv\Dropzone;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Lybuneiv\Dropzone\Facades\Dropzone;
use Illuminate\Routing\Router;

class DropzoneServiceProvider extends ServiceProvider
{

	protected $viewsPath = 'resources/views/';
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		// $this->loadRoutesFrom(__DIR__.'/routes.php');
		// $this->loadViewsFrom(__DIR__.'/views', 'Dropzone');
		$this->publishAll();
        $this->registerViews();
        $this->registerRoutes();

	}
	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('dropzone', function($app) {
	      return new Dropzone();
	  	});

		$this->viewsPath = (!empty(config('dropzone.views')))
            ? base_path('resources/views/vendor/dropzone/')
            : $this->packagePath($this->viewsPath);

        $this->app->make('Lybuneiv\Dropzone\Controllers\ImageController');
	}


	protected function publishAll()
    {
        $this->publishes([
            $this->packagePath('resources/views') => base_path('resources/views/vendor/dropzone/'),
        ], 'views');

        $this->publishes([
            $this->packagePath('resources/assets') => public_path('vendor/lybuneiv/dropzone'),
        ], 'public');

        $this->publishes([
            $this->packagePath('config/config.php') => config_path('dropzone.php'),
        ], 'config');
    }

    protected function registerViews()
    {
        if(!config('dropzone.views')) {
            $this->loadViewsFrom($this->viewsPath, 'Dropzone');
        } else {
        	// dd('s');
        	$this->loadViewsFrom(base_path('resources/views/vendor/dropzone/'), 'Dropzone');
            $this->publishes([
                $this->packagePath('resources/views') => base_path('resources/views/vendor/dropzone/'),
            ], 'views');
        }
    }

    /**
     * Register the package routes
     *
     * @warn consider allowing routes to be disabled
     * @see http://laravel.com/docs/master/routing
     * @see http://laravel.com/docs/master/packages#routing
     * @return void
     */
    protected function registerRoutes()
    {
    	// $this->loadRoutesFrom(__DIR__.'/routes.php');

        $this->app['router']->group([
            'namespace' => 'Lybuneiv\Dropzone\Controllers',
            // 'middleware' => ['web'],
            'prefix' => 'admin'
        ], function(Router $router) {
        	// $this->loadRoutesFrom(__DIR__.'/routes.php');
        	$router->post('image/upload-multiple/{name?}', 'ImageController@uploadMultiple')->name('image_upload_multiple');
			$router->post('image/upload-single/{name?}', 'ImageController@uploadSingle')->name('image_upload_single');
			$router->post('image/upload', 'ImageController@store')->name('image_upload');

            // $router->resource('staff', 'Lybuneiv\Staff\Controllers\StaffController', ['names' => 'staff']);
        });
    }

    /**
     * Loads a path relative to the package base directory
     *
     * @param string $path
     * @return string
     */
    protected function packagePath($path = '')
    {
        return sprintf("%s/../%s", __DIR__ , $path);
    }
}