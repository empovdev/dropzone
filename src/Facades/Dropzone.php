<?php

namespace Lybuneiv\Dropzone\Facades;

class Dropzone
{

    /**
     * Render dropzone css
     * @return view as string
     */
    public function renderDropzoneCss()
    {
        return view('Dropzone::dropzone_css');
    }

    /**
     * Render dropzone js
     * @return view as string
     */
    public function renderDropzoneJs()
    {
        return view('Dropzone::dropzone_js');
    }

   /**
     * Render dropzone html
     * @return view as string
     */
    public function renderSingle($image_single = '', $input_name = 'photo', $class = 'dropzone_single')
    {
        $data['class_name'] = $class;
        $data['image_single'] = $image_single;
        $data['input_name'] = $input_name;
        return view('Dropzone::single', $data);
    }

    /**
     * Render dropzone js
     * @return view as string
     */
    public function renderSingleJs($class = '')
    {
        $data['class_name'] = $class;
        return view('Dropzone::single_js', $data);
    }

    /**
     * Render dropzone html
     * @return view as string
     */
    public function renderMultiple($images = [], $input_name = 'photo', $class = 'dropzone_single')
    {
        $data['class_name'] = $class;
        $data['items'] = $images;
        $data['input_name'] = $input_name;
        $data['count'] = count($images);
        return view('Dropzone::multiple', $data);
    }

    /**
     * Render dropzone js
     * @return view as string
     */
    public function renderMultipleJs($class = '')
    {
        $data['class_name'] = $class;
        return view('Dropzone::multiple_js', $data);
    }
}
