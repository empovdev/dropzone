<div class="card card-default mydropzone dev_{{ $class_name }}">
    {{ Form::hidden($input_name, $image_single, ['class' => 'photo']) }}
    <div class="card-header">
        <button class="photo-info btn btn-default form-control" type="button" onclick="triggerAddImageSingle('{{ $class_name }}')">{{ __('Add Photo') }}</button>
    </div>
    <div class="card-body single_dropzone">
        <div class="dropzone_single {{ $class_name }}">

            <div class="dz-preview dz-processing dz-image-preview dz-success dz-complete exist_image">
                <div class="dz-image">
                    @if($image_single)
                        <img data-dz-thumbnail="" src="{{ $image_single }}" />
                    @else
                        <img data-dz-thumbnail="" src="{{ asset('vendor/lybuneiv/dropzone/img/default.jpg') }}" style="width: 100%;" />
                    @endif
                </div>
                <div class="dz-details">
                    <div class="dz-filename"><span data-dz-name>
                        @if(!$image_single)
                            {{ "default.png" }}
                        @endif
                    </span></div>
                    <div class="dz-size" data-dz-size></div>
                </div>
                <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                <div class="dz-success-mark"><span></span></div>
                <div class="dz-error-mark"><span></span></div>
                <div class="dz-error-message"><span data-dz-errormessage></span></div>
                @if($image_single)
                    <a data-name="{{ $image_single }}" class="dz-remove" data-dz-remove="" onClick="removeImageDropzone(this)">{{ __('Remove file') }}</a>
                    <div class="input-field col s12">
                        <input type="text" value="{{ $caption1 ?? '' }}" name="img_caption1" onblur="addCaptionImage(this,'hidden_caption1')">
                        <label>Caption 1</label>
                    </div>
                    <div class="input-field col s12">
                        <input type="text" value="{{ $caption2 ?? '' }}" name="img_caption2" onblur="addCaptionImage(this,'hidden_caption2')">
                        <label>Caption 2</label>
                    </div>
                @endif
            </div>


            <div class="fallback">
                <input name="file" type="file"/>
            </div>
        </div>


    </div>
</div>
{{ Form::hidden('base_url', url('/'), ['class' => 'base_url']) }}
