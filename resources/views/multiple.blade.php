<div class="card card-default mydropzone dev_{{ $class_name }}">
    {{ Form::hidden('count_item', $count_item ?? 0, ['class' => 'count_item']) }}
    {{ Form::hidden('input_name', $input_name, ['class' => 'input_name']) }}
    <div class="card-header">
        <button class="photo-info btn btn-default form-control" type="button" onclick="triggerAddImageMultiple('{{ $class_name }}')">{{ __('Add Photo') }}</button>
    </div>
    <div class="card-body multiple_dropzone">
        <div class="{{ $class_name }}">

            @if(isset($items) && $count > 0)
                    @foreach($items as $val)
                        <div class="dz-preview dz-processing dz-image-preview dz-success dz-complete">
                            <div class="dz-image">
                                <img data-dz-thumbnail="" src="{{ $val['image'] ?? '' }}" />
                            </div>
                            <div class="dz-details">
                                <div class="dz-filename">
                                    <span data-dz-name>
                                        <label>
                                            @if($val['main'])
                                                {{ Form::radio('main', $val['image'] ?? '', true, ['onChange' => 'changeMain(this);']) }}
                                            @else
                                                {{ Form::radio('main', $val['image'] ?? '', false, ['onChange' => 'changeMain(this);']) }}
                                            @endif
                                            {{ __('Main') }}
                                        </label>
                                    </span>
                                </div>
                                <div class="dz-size" data-dz-size></div>
                            </div>
                            <div class="dz-progress">
                                <span class="dz-upload" data-dz-uploadprogress></span>
                            </div>
                            <div class="dz-success-mark"><span></span></div>
                            <div class="dz-error-mark"><span></span></div>
                            <div class="dz-error-message"><span data-dz-errormessage></span></div>
                            <div class="hidden_file" data-value="{{ $val['image'] ?? '' }}">
                                <input type="hidden" value="" name="img_id[]">
                                <input type="hidden" value="'+value['name']+'" name="{{ $input_name }}[]" class="hidden_value">
                            </div>

                            {{-- <a data-name="{{ $val['image'] ?? '' }}" class="dz-remove" data-dz-remove="" onclick="removeImageDropzone(this)">{{ __('Remove file') }}</a>
                            <div class="input-field col s12">
                                <input data-class="hidden_multiple_caption1" type="text" value="{{ $val->caption1 }}" name="tem_img_caption1[]" class="tmp_hidden_caption1" onblur="addCaptionImageMutiple(this)" data-name="{{ $val->name }}" />
                                <label>{{ __('Caption 1') }}</label>
                            </div>
                            <div class="input-field col s12">
                                <input data-class="hidden_multiple_caption2" type="text" value="{{ $val->caption2 }}" name="tem_img_caption2[]" class="tmp_hidden_caption2" onblur="addCaptionImageMutiple(this)" data-name="{{ $val->name }}" />
                                <label>{{ __('Caption 2') }}</label>
                            </div> --}}
                        </div> 
                    @endforeach
            @else
                <div class="dz-message" data-dz-message><span>Drop files here to upload</span></div>
            @endif

            <div class="fallback">
                <input name="file" type="file"/>
            </div>
        </div>


    </div>
</div>
{{ Form::hidden('base_url', url('/'), ['class' => 'base_url']) }}
