<script type="text/javascript">
    // project base url
    $(function(){
        var class_name = '{{ $class_name }}';
        var el = $('.'+class_name);
        var option = generateOptionMutiple(el, class_name);
        el.dropzone(option);
    });

</script>
