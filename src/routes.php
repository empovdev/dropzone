<?php

Route::post('image/upload-multiple', 'ImageController@uploadMultiple')->name('image_upload_multiple');
Route::post('image/upload-single', 'ImageController@uploadSingle')->name('image_upload_single');
Route::post('image/upload', 'ImageController@store')->name('image_upload');